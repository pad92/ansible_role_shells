# Managed by Ansible

# System-wide .bashrc file for interactive bash(1) shells.

# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
	        # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi

### Custom :
TTY=$(tty | cut -d/ -f3,4)

# Set the default editor
export EDITOR=vim
export VISUAL=vim

# History
export HISTTIMEFORMAT="[%F %T] "
export HISTFILE=~/.bash_history
export HISTFILESIZE=1000000
export HISTSIZE=1000000
export HISTIGNORE=""
export HISTCONTROL=ignoreboth
readonly HISTFILE
readonly HISTSIZE
readonly HISTFILESIZE
readonly HISTIGNORE
readonly HISTCONTROL

shopt -s cmdhist
shopt -s histappend

PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND ; }"'echo $$ - $SSH_USER - $USER[$$] - $SSH_CONNECTION - $TTY - "$(history 1)" | logger -t "bash_history"'
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

# You may uncomment the following lines if you want `ls' to be colorized:
export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'

alias rgrep='grep -r'

alias rsync-copy='rsync --verbose --progress --human-readable --compress --archive --hard-links --one-file-system'
alias rsync-move='rsync --verbose --progress --human-readable --compress --archive --hard-links --one-file-system --remove-source-files'
alias rsync-synchronize='rsync --verbose --progress --human-readable --compress --archive --hard-links --one-file-system --update --delete'
alias rsync-update='rsync --verbose --progress --human-readable --compress --archive --hard-links --one-file-system --update'

alias du_sort="du -S | sort -n"
alias ps_cpu="/bin/ps -eo pcpu,pid,user,args | sort -k 1 -r"

curl_time() {
    CURL_FORMAT='                  http_code:  %{http_code}
            time_namelookup:  %{time_namelookup} ms
               time_connect:  %{time_connect} ms
            time_appconnect:  %{time_appconnect} ms
           time_pretransfer:  %{time_pretransfer} ms
              time_redirect:  %{time_redirect} ms
         time_starttransfer:  %{time_starttransfer} ms
                            ----------
                 time_total:  %{time_total} ms\n'
  curl -w "$CURL_FORMAT" -o /dev/null -s "$@"
}

who
